import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import axios from 'axios';

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };

        this.submitHandler = this.submitHandler.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    submitHandler(event) {
        const name = event.target.name;

        if (name === 'email') {
            this.setState({
                email: event.target.value
            });
        }

        if (name === 'password') {
            this.setState({
                password: event.target.value
            });
        }

    }

    handleSubmit(event) {
        event.preventDefault();

        const person = {
            email: this.state.email,
            password: this.state.password
        }

        axios({
            method: 'post',
            url: 'http://localhost:3000/login',
            data: person
        })
        .then((response) => {
            this.setState({
                login: true
            });
        })
        .catch((error) => {
            console.log(error);
        });
    }

    render() {

        const loginTrue = this.state.login;
        if (loginTrue) {
            return <Redirect exact to='/profile' />;
        }

        return (
            <div className='box'>
                <h1 className='mb-3'>Login</h1>
                <form action='/login' onSubmit={this.handleSubmit}>
                    <div className='form-group'>
                        <label htmlFor='email'>Email:</label>
                        <input type="email" id='email' name='email' value={this.state.value} onChange={this.submitHandler} className='form-control' required />
                    </div>
                    <div className='form-group'>
                        <label htmlFor='password'>Password:</label>
                        <input type="password" id='password' name='password' value={this.state.value} onChange={this.submitHandler} className='form-control' required />
                    </div>
                    <input type="submit" value="Submit" className='btn btn-primary float-right' />
                </form>
            </div>
        );
    };
}

export default Login;