import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import axios from 'axios';

import '../../App.css'


class Registration extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fullName: '',
            email: '',
            username: '',
            password: ''
        };

        this.inputChangeHandler = this.inputChangeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
    }

    inputChangeHandler = (event) => {
        const name = event.target.name;

        if (name === 'fullName') {
            this.setState({
                fullName: event.target.value
            });
        }

        if (name === 'email') {
            this.setState({
                email: event.target.value
            });
        }

        if (name === 'username') {
            this.setState({
                username: event.target.value
            });
        }

        if (name === 'password') {
            this.setState({
                password: event.target.value
            });
        }
    }

    submitHandler = (event) => {
        event.preventDefault();
        
        const person = {
            fullName: this.state.fullName,
            email: this.state.email,
            username: this.state.username,
            password: this.state.password            
        }
        
        axios({
            method: 'post',
            url: 'http://localhost:3000/register',
            data: person
        })
        .then((response) => {
            console.log(response);
            this.setState({
                register: true
            });
        })
        .catch((error) => {
            console.log(error);
        });
    }

    render() {

        const registerTrue = this.state.register;
        if (registerTrue){
            return <Redirect exact to='/profile' />;
        }

        return (
            <div className='box'>
                <h1 className='mb-3'>Registration</h1>
                <form action='/register' onSubmit={this.submitHandler}>
                    <div className='form-group'>
                        <label htmlFor='fullName'>Full Name:</label>
                        <input type="text" id='fullName' name="fullName" value={this.state.value} onChange={this.inputChangeHandler} className='form-control' required />
                    </div>
                    <div className='form-group'>
                        <label htmlFor='email'>Email:</label>
                        <input type="email" id='email' name='email' value={this.state.value} onChange={this.inputChangeHandler} className='form-control' required />
                    </div>
                    <div className='form-group'>
                        <label htmlFor='username'>Username:</label>
                        <input type="text" id='username' name="username" value={this.state.value} onChange={this.inputChangeHandler} className='form-control' required />
                    </div>
                    <div className='form-group'>
                        <label htmlFor='password'>Password:</label>
                        <input type="password" id='password' name='password' value={this.state.value} onChange={this.inputChangeHandler} className='form-control' required />
                    </div>
                    <input type="submit" value="Submit" className='btn btn-primary float-right' />
                </form>
            </div>
        );
    };
}

export default Registration;