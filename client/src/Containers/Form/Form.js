import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import Login from '../../Components/Login/Login';
import Registration from '../../Components/Registration/Registration';
import Profile from '../../Components/Profile/Profile';



class Form extends Component {
    render() {
        return (
            <Router>
                <div className='home container'>
                    <div className='row text-center justify-content-center'>
                        <Link to="/login" className='col-6 col-lg-6 mt-2 mb-4 btn btn-nav'>
                            Login
                        </Link>
                        <Link to="/registration" className='col-6 col-lg-6 mt-2 mb-4 btn btn-nav'>
                            Registration
                        </Link>

                        <Route exact path="/login" component={Login} />
                        <Route exact path="/registration" component={Registration} />
                        <Route exact path="/profile" component={Profile} />

                    </div>
                </div>
            </Router>
        );
    };
}

export default Form;