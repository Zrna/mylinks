## MyLinks

Application on which you can save more links and share just one so your friends can get a list of your links/sources/pages

## Start application

`npm run migration` to create table/tables in database

`npm start` to run application
