CREATE DATABASE my_links;

CREATE TABLE if not exists users (
                        id INT AUTO_INCREMENT PRIMARY KEY,
                        full_name VARCHAR(255),
                        email VARCHAR(255) NOT NULL,
                        username VARCHAR(255),
                        password VARCHAR(255),
                        created_at TIMESTAMP DEFAULT NOW(),
                        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
                    );