var config = require('../config');
var mysql = require('mysql');
var connection = mysql.createPool(config.databaseOptions);

var migration = require('mysql-migrations');

migration.init(connection, __dirname + '/migrations');

// creating 'users' table
let createUsersTable = `CREATE TABLE if not exists users (
                        id INT AUTO_INCREMENT PRIMARY KEY,
                        fullName VARCHAR(255),
                        email VARCHAR(255) NOT NULL,
                        username VARCHAR(255),
                        password VARCHAR(255),
                        created_at TIMESTAMP DEFAULT NOW(),
                        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
                    )`;

module.exports = {
    "up": connection.query(createUsersTable, function (error, results) {
        if (error) throw error;
        console.log(results);
        console.log('Table "users" created.');
    }),
    "down": ""
}
