module.exports.databaseOptions = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'my_links',
    timezone: 'UTC+1',
    multipleStatements: true
};