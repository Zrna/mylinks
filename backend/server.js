var config = require('../database/config');
var mysql = require('mysql');
var connection = mysql.createConnection(config.databaseOptions);

var express = require("express");
var bodyParser = require('body-parser');
var app = express();
const bcrypt = require('bcrypt');

app.set('view engine', 'ejs');

//app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json())

app.get('/', (req, res) => res.send('Hello World!'))

app.post('/register', function (req, res) {
    var person = {
        fullName: req.body.fullName,
        email: req.body.email,
        username: req.body.username,
        password: bcrypt.hashSync(req.body.password, 10)
    }
    console.log(`===== server.js\nFull name: ${person.fullName} | Email: ${person.email} | Username: ${person.username} | Password: ${person.password}`);

    let sqlCheckEmail = 'SELECT * FROM users WHERE email = ? OR username = ? LIMIT 1';

    connection.query(sqlCheckEmail, [person.email, person.username], function (error, results) {
        if (results.length) {
            console.log('Email ' + person.email + ' or username ' + person.username + ' already exists.\n=====');
        } else {
            connection.query('INSERT INTO users SET ?', person, function (err, result) {
                if (err) throw err;
                res.redirect('/');
                console.log('successful registration');
            });
            console.log('\nEmail ' + person.email + ' has been added to database.\n=====');
        }
    });
});

app.post('/login', function (req, res) {
    var person = {
        email: req.body.email,
        password: req.body.password
    }
    
    let sqlCheckEmail = 'SELECT * FROM users WHERE email = ?';
    
    connection.query(sqlCheckEmail, person.email, function (error, results) {
        if (results.length) {
            const userId = results[0].id;
            const hash = results[0].password;
            const compareEnteredPass = bcrypt.compareSync(req.body.password, hash);
            if (compareEnteredPass) {
                res.redirect('/');
                console.log(`Id: ${userId} | Email: ${person.email} | Password: ${compareEnteredPass}`);
            } else if (!compareEnteredPass) {
                console.log('Password is not correct')
            } else {
                console.log('something went wrong')
            }
            
        } else {
            if (err) throw err;
        }
    });
});


app.listen(3001, function () {
    console.log('Server running on localhost:3001\n');
});